import React from 'react'
import { storiesOf, action } from '@kadira/storybook'

import CharacterBioPage from '../src/components/bioPages/character_bio_page'
import { characterData } from '../test/testHelpers/test_api'

const result = {
  data: characterData
}
const loadingProps = {loading: true}
const errorProps = {loading: false, error: 'It broke :('}
const successProps = {loading: false, error: false, result}

storiesOf('Character Bio Page', module)
  .add('Initial state', () => <CharacterBioPage />)
  .add('Loading', () => <CharacterBioPage {...loadingProps} />)
  .add('Error', () => <CharacterBioPage {...errorProps} />)
  .add('Success', () => <CharacterBioPage {...successProps} />)
