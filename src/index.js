import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory } from 'react-router';
import reducers from './reducers';
import routes from './routes';
import api from './api/api';
import '../style/style.less';
import thunk from 'redux-thunk';

const createStoreWithMiddleware = applyMiddleware(
  thunk.withExtraArgument({ api })
)(createStore);

ReactDOM.render(
  <Provider store={ createStoreWithMiddleware(reducers) }>
    <Router history={ browserHistory } routes={ routes } />
  </Provider>,
  document.querySelector('.container')
);
