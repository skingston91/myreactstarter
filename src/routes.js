import React from 'react';
import { Route } from 'react-router';

import App from './components/App';
import BioPage from './components/Bio';

export default (
  <Route path="/" components={ App }>
    <Route path="bio/:type/:id" component={ BioPage } />
  </Route>
);
