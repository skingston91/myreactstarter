import React from 'react';
import { Link } from 'react-router';

const ObjectLink = ({ url, children }) => {
  const matches = url && url.match(/swapi\.co\/api\/([^/]+)\/(\d+)\/?$/);
  return matches
    ? <Link to={ `/bio/${ matches[1] }/${ matches[2] }` }>
        { ' ' }{ children }{ ' ' }
      </Link>
    : children;
};

ObjectLink.propTypes = {
  url: React.PropTypes.string,
  children: React.PropTypes.object,
};

export default ObjectLink;
