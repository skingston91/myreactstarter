import React from 'react';
import ObjectLink from '../ObjectLink';

const CharacterBioPage = ({ result, error, loading }) => {
  if (error) {
    return (
      <p>
        { 'Error!' }
      </p>
    );
  }
  if (loading) {
    return (
      <p>
        { 'Loading your data now!' }
      </p>
    );
  }
  if (!result) return <div />;
  const {
    name,
    height,
    mass,
    hairColor,
    gender,
    birthYear,
    homeworld,
    films,
    species,
    vehicles,
    starships,
  } = result.data;

  return (
    <div>
      <div>
        <h3>
          { ' ' }Character: { name }{ ' ' }
        </h3>
        <div>
          <table className="">
            <tbody>
              <tr>
                <th> Facts: </th>
              </tr>
              <tr>
                <th> Height: </th>
                <td>
                  { ' ' }{ height }{ ' ' }
                </td>
              </tr>
              <tr>
                <th> Mass </th>
                <td>
                  { ' ' }{ mass }{ ' ' }
                </td>
              </tr>
              <tr>
                <th> Gender </th>
                <td>
                  { ' ' }{ gender }{ ' ' }
                </td>
              </tr>
              <tr>
                <th> Hair Colour: </th>
                <td>
                  { ' ' }{ hairColor }{ ' ' }
                </td>
              </tr>
              <tr>
                <th> Birth Year: </th>
                <td>
                  { ' ' }{ birthYear }{ ' ' }
                </td>
              </tr>
              <tr>
                <th> Homeworld: </th>
                <td>
                  <ObjectLink url={ homeworld }>
                    <p>
                      { homeworld }
                    </p>
                  </ObjectLink>
                </td>
              </tr>
              <tr>
                <th> Films: </th>
                { films.map(film =>
                  <td>
                    <ObjectLink url={ film }>
                      <p>
                        { film }
                      </p>
                    </ObjectLink>
                  </td>
                ) }
              </tr>
              <tr>
                <th> Species: </th>
                { species.map(specie =>
                  <td>
                    <ObjectLink url={ specie }>
                      <p>
                        { specie }
                      </p>
                    </ObjectLink>
                  </td>
                ) }
              </tr>
              <tr>
                <th> Vehicles: </th>
                { vehicles.map(vehicle =>
                  <td>
                    <ObjectLink url={ vehicle }>
                      <p>
                        { vehicle }
                      </p>
                    </ObjectLink>
                  </td>
                ) }
              </tr>
              <tr>
                <th> Starships: </th>
                { starships.map(starship =>
                  <td>
                    <ObjectLink url={ starship }>
                      <p>
                        { starship }
                      </p>
                    </ObjectLink>
                  </td>
                ) }
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

CharacterBioPage.propTypes = {
  result: React.PropTypes.object,
  error: React.PropTypes.bool,
  loading: React.PropTypes.bool,
};

export default CharacterBioPage;
