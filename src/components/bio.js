import React from 'react';
import CharacterBioPage from './bioPages/CharacterBioPage';
import FilmBioPage from './bioPages/FilmBioPage';
import BioDataContainer from '../containers/BioDataContainer';

function getRendererByType({ type }) {
  switch (type) {
    case 'films':
      return FilmBioPage;
    case 'people':
      return CharacterBioPage;
    default:
      throw Error(`Don't know how to render a '${ type }'`);
  }
}

const Bio = ({ routeParams }) =>
  <BioDataContainer { ...routeParams } render={ getRendererByType(routeParams) } />;

Bio.propTypes = {
  routeParams: React.PropTypes.object,
};

export default Bio;

/*
import React from 'react'
import CharacterBioPage from './bioPages/character_bio_page'
import FilmBioPage from './bioPages/film_bio_page'
import BioDataContainer from '../containers/bio_data_container'

export default (props) => {
  const { route, result, loading, error } = props
  if (error) {
    return <p>{'Error!'}</p>
  }
  if (loading) {
    return <p>{'Loading your data now!'}</p>
  }
  // return (<BioDataContainer type={this.props.routeParams.type} id={this.props.routeParams.id} />)
  switch (this.props.routeParams.type) {
    case 'films': return <BioDataContainer {...result} render={FilmBioPage} type={this.props.routeParams.type} id={this.props.routeParams.id} />
    case 'people': return <CharacterBioPage {...result} render={FilmBioPage} type={this.props.routeParams.type} id={this.props.routeParams.id} />
    default: <UnknownTypeBioPage type={route.params.type} {...result} />
  }
}
*/
