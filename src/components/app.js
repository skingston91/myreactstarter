import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>
        React simple starter
        { this.props.children }
      </div>
    );
  }
}

export default App;

App.propTypes = {
  children: React.PropTypes.object,
};

/*
import React from 'react';

const App = () =>
  <div>
    React simple starter
    { this.props.children }
  </div>;

App.propTypes = {
  children: React.PropTypes.object,
};

export default App;
*/
