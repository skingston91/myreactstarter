/**
 * Usage
 * ===
 * <BioDataContainer render={SomeOtherComponent} type={type} id={id} />
 *
 * This will render <SomeOtherComponent data={{ result, error, loading }} type={type} id={id} />
 **/

import React from 'react';
import { connect } from 'react-redux';
import { fetchData } from '../actions';

export class BioDataContainer extends React.Component {
  componentWillMount() {
    this.loadData(this.props);
  }
  componentWillReceiveProps(newProps) {
    this.loadData(newProps);
  }
  loadData({ data: { result, error, loading }, type, id }) {
    if (result || error || loading) return;
    return this.props.fetchData('api', { type, id });
  }
  render() {
    const { render: Component, data } = this.props;
    return <Component { ...data } />;
  }
}

BioDataContainer.propTypes = {
  data: React.PropTypes.shape({
    result: React.PropTypes.any,
    error: React.PropTypes.any,
    loading: React.PropTypes.bool,
  }).isRequired,
  type: React.PropTypes.oneOf(['films', 'people']).isRequired,
  id: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
  ]).isRequired,
};

function mapStateToProps({ bio }, { type, id }) {
  return {
    data: (bio && bio[type] && bio[type][id]) || {},
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: (apiName, object) => dispatch(fetchData(apiName, object)),
  };
}

BioDataContainer.propTypes = {
  fetchData: React.PropTypes.func,
  render: React.PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(BioDataContainer);
