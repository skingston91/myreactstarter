import { combineReducers } from 'redux';
import bioReducer from './reducer_bio';

const rootReducer = combineReducers({
  bio: bioReducer,
});

export default rootReducer;
