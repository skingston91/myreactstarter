// Creates all the api requests
import axios from 'axios';

const ROOT_URL = 'https://swapi.co/api';

export function fetchData({ type, id = '' }) {
  return axios.get(`${ ROOT_URL }/${ type }/${ id }`);
}

export function fetchCharacter(id) {
  return fetchData({ type: 'people', id });
}
export function fetchCharacters() {
  return fetchData({ type: 'people' });
}
export function fetchPlanet(id) {
  return fetchData({ type: 'planets', id });
}
export function fetchStarship(id) {
  return fetchData({ type: 'starships', id });
}

export default {
  fetchData,
  fetchCharacter,
  fetchCharacters,
  fetchPlanet,
  fetchStarship,
};
