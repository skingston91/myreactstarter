export function fetchCharacter(itemNumber) {
  return Promise.resolve(characterData);
}

export function fetchCharacters() {
  return Promise.resolve(charactersData);
}

export function fetchPlanet(itemNumber) {
  return Promise.resolve(planetData);
}

export function fetchStarship(itemNumber) {
  return Promise.resolve(starshipData);
}

export default {
  fetchCharacter,
  fetchCharacters,
  fetchPlanet,
  fetchStarship,
  charactersData,
};

export const characterData = {
  name: 'Luke Skywalker',
  height: '172',
  mass: '77',
  hairColor: 'blond',
  skin_color: 'fair',
  eye_color: 'blue',
  birthYear: '19BBY',
  gender: 'male',
  homeworld: 'http://swapi.co/api/planets/1/',
  films: [
    'http://swapi.co/api/films/6/',
    'http://swapi.co/api/films/3/',
    'http://swapi.co/api/films/2/',
    'http://swapi.co/api/films/1/',
    'http://swapi.co/api/films/7/',
  ],
  species: ['http://swapi.co/api/species/1/'],
  vehicles: [
    'http://swapi.co/api/vehicles/14/',
    'http://swapi.co/api/vehicles/30/',
  ],
  starships: [
    'http://swapi.co/api/starships/12/',
    'http://swapi.co/api/starships/22/',
  ],
  created: '2014-12-09T13:50:51.644000Z',
  edited: '2014-12-20T21:17:56.891000Z',
  url: 'http://swapi.co/api/people/1/',
};

const starshipData = {
  name: 'Death Star',
  model: 'DS-1 Orbital Battle Station',
  manufacturer:
    'Imperial Department of Military Research, Sienar Fleet Systems',
  cost_in_credits: '1000000000000',
  length: '120000',
  max_atmosphering_speed: 'n/a',
  crew: '342953',
  passengers: '843342',
  cargo_capacity: '1000000000000',
  consumables: '3 years',
  hyperdrive_rating: '4.0',
  MGLT: '10',
  starship_class: 'Deep Space Mobile Battlestation',
  pilots: [],
  films: ['http://swapi.co/api/films/1/'],
  created: '2014-12-10T16:36:50.509000Z',
  edited: '2014-12-22T17:35:44.452589Z',
  url: 'http://swapi.co/api/starships/9/',
};

const planetData = {
  name: 'Yavin IV',
  rotation_period: '24',
  orbital_period: '4818',
  diameter: '10200',
  climate: 'temperate, tropical',
  gravity: '1 standard',
  terrain: 'jungle, rainforests',
  surface_water: '8',
  population: '1000',
  residents: [],
  films: ['http://swapi.co/api/films/1/'],
  created: '2014-12-10T11:37:19.144000Z',
  edited: '2014-12-20T20:58:18.421000Z',
  url: 'http://swapi.co/api/planets/3/',
};

export const charactersData = {
  count: 87,
  next: 'http://swapi.co/api/people/?page=2',
  previous: null,
  results: [
    {
      name: 'Luke Skywalker',
      height: '172',
      mass: '77',
      hairColor: 'blond',
      skin_color: 'fair',
      eye_color: 'blue',
      birthYear: '19BBY',
      gender: 'male',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: [
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
        'http://swapi.co/api/films/7/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: [
        'http://swapi.co/api/vehicles/14/',
        'http://swapi.co/api/vehicles/30/',
      ],
      starships: [
        'http://swapi.co/api/starships/12/',
        'http://swapi.co/api/starships/22/',
      ],
      created: '2014-12-09T13:50:51.644000Z',
      edited: '2014-12-20T21:17:56.891000Z',
      url: 'http://swapi.co/api/people/1/',
    },
    {
      name: 'C-3PO',
      height: '167',
      mass: '75',
      hairColor: 'n/a',
      skin_color: 'gold',
      eye_color: 'yellow',
      birthYear: '112BBY',
      gender: 'n/a',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: [
        'http://swapi.co/api/films/5/',
        'http://swapi.co/api/films/4/',
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
      ],
      species: ['http://swapi.co/api/species/2/'],
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:10:51.357000Z',
      edited: '2014-12-20T21:17:50.309000Z',
      url: 'http://swapi.co/api/people/2/',
    },
    {
      name: 'R2-D2',
      height: '96',
      mass: '32',
      hairColor: 'n/a',
      skin_color: 'white, blue',
      eye_color: 'red',
      birthYear: '33BBY',
      gender: 'n/a',
      homeworld: 'http://swapi.co/api/planets/8/',
      films: [
        'http://swapi.co/api/films/5/',
        'http://swapi.co/api/films/4/',
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
        'http://swapi.co/api/films/7/',
      ],
      species: ['http://swapi.co/api/species/2/'],
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:11:50.376000Z',
      edited: '2014-12-20T21:17:50.311000Z',
      url: 'http://swapi.co/api/people/3/',
    },
    {
      name: 'Darth Vader',
      height: '202',
      mass: '136',
      hairColor: 'none',
      skin_color: 'white',
      eye_color: 'yellow',
      birthYear: '41.9BBY',
      gender: 'male',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: [
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: [],
      starships: ['http://swapi.co/api/starships/13/'],
      created: '2014-12-10T15:18:20.704000Z',
      edited: '2014-12-20T21:17:50.313000Z',
      url: 'http://swapi.co/api/people/4/',
    },
    {
      name: 'Leia Organa',
      height: '150',
      mass: '49',
      hairColor: 'brown',
      skin_color: 'light',
      eye_color: 'brown',
      birthYear: '19BBY',
      gender: 'female',
      homeworld: 'http://swapi.co/api/planets/2/',
      films: [
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
        'http://swapi.co/api/films/7/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: ['http://swapi.co/api/vehicles/30/'],
      starships: [],
      created: '2014-12-10T15:20:09.791000Z',
      edited: '2014-12-20T21:17:50.315000Z',
      url: 'http://swapi.co/api/people/5/',
    },
    {
      name: 'Owen Lars',
      height: '178',
      mass: '120',
      hairColor: 'brown, grey',
      skin_color: 'light',
      eye_color: 'blue',
      birthYear: '52BBY',
      gender: 'male',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: [
        'http://swapi.co/api/films/5/',
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/1/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:52:14.024000Z',
      edited: '2014-12-20T21:17:50.317000Z',
      url: 'http://swapi.co/api/people/6/',
    },
    {
      name: 'Beru Whitesun lars',
      height: '165',
      mass: '75',
      hairColor: 'brown',
      skin_color: 'light',
      eye_color: 'blue',
      birthYear: '47BBY',
      gender: 'female',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: [
        'http://swapi.co/api/films/5/',
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/1/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:53:41.121000Z',
      edited: '2014-12-20T21:17:50.319000Z',
      url: 'http://swapi.co/api/people/7/',
    },
    {
      name: 'R5-D4',
      height: '97',
      mass: '32',
      hairColor: 'n/a',
      skin_color: 'white, red',
      eye_color: 'red',
      birthYear: 'unknown',
      gender: 'n/a',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: ['http://swapi.co/api/films/1/'],
      species: ['http://swapi.co/api/species/2/'],
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:57:50.959000Z',
      edited: '2014-12-20T21:17:50.321000Z',
      url: 'http://swapi.co/api/people/8/',
    },
    {
      name: 'Biggs Darklighter',
      height: '183',
      mass: '84',
      hairColor: 'black',
      skin_color: 'light',
      eye_color: 'brown',
      birthYear: '24BBY',
      gender: 'male',
      homeworld: 'http://swapi.co/api/planets/1/',
      films: ['http://swapi.co/api/films/1/'],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: [],
      starships: ['http://swapi.co/api/starships/12/'],
      created: '2014-12-10T15:59:50.509000Z',
      edited: '2014-12-20T21:17:50.323000Z',
      url: 'http://swapi.co/api/people/9/',
    },
    {
      name: 'Obi-Wan Kenobi',
      height: '182',
      mass: '77',
      hairColor: 'auburn, white',
      skin_color: 'fair',
      eye_color: 'blue-gray',
      birthYear: '57BBY',
      gender: 'male',
      homeworld: 'http://swapi.co/api/planets/20/',
      films: [
        'http://swapi.co/api/films/5/',
        'http://swapi.co/api/films/4/',
        'http://swapi.co/api/films/6/',
        'http://swapi.co/api/films/3/',
        'http://swapi.co/api/films/2/',
        'http://swapi.co/api/films/1/',
      ],
      species: ['http://swapi.co/api/species/1/'],
      vehicles: ['http://swapi.co/api/vehicles/38/'],
      starships: [
        'http://swapi.co/api/starships/48/',
        'http://swapi.co/api/starships/59/',
        'http://swapi.co/api/starships/64/',
        'http://swapi.co/api/starships/65/',
        'http://swapi.co/api/starships/74/',
      ],
      created: '2014-12-10T16:16:29.192000Z',
      edited: '2014-12-20T21:17:50.325000Z',
      url: 'http://swapi.co/api/people/10/',
    },
  ],
};
